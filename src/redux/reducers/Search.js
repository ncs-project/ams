import { INPUT_CHANGE } from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
    result: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case INPUT_CHANGE: {
            return {
                ...state,
                result: action.payload,
            };
        }
        default:
            return state;
    }
};
