import { INPUT_CHANGE } from '../../@jumbo/constants/ActionTypes';

export const inputChangeSearch = key => {
  return dispatch => {
    dispatch({
      type: INPUT_CHANGE,
      payload: key,
    });
  };
};


