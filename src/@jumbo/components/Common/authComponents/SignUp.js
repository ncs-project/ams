import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Box } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { AuhMethods } from '../../../../services/auth';
import { alpha, makeStyles } from '@material-ui/core/styles';
import CmtImage from '../../../../@coremat/CmtImage';
import Typography from '@material-ui/core/Typography';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import AuthWrapper from './AuthWrapper';
import { NavLink } from 'react-router-dom';
import { Checkbox, Form, Input, Row, message} from 'antd';
import AuthService from 'services/auth/AuthService';
import { createBrowserHistory } from 'history';
import { rootPath } from 'helpers/buildUrl';
const useStyles = makeStyles(theme => ({
  authThumb: {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    [theme.breakpoints.up('md')]: {
      width: '50%',
      order: 2,
    },
  },
  authContent: {
    padding: 30,
    [theme.breakpoints.up('md')]: {
      width: props => (props.variant === 'default' ? '50%' : '100%'),
      order: 1,
    },
    [theme.breakpoints.up('xl')]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  textFieldRoot: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: alpha(theme.palette.common.dark, 0.12),
    },
  },
  textCapital: {
    textTransform: 'capitalize',
  },
  textAcc: {
    textAlign: 'center',
    '& a': {
      marginLeft: 4,
    },
  },
  alrTextRoot: {
    textAlign: 'center',
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right',
    },
  },
}));

const SignUp = ({ method = CurrentAuthMethod, variant = 'default', wrapperVariant = 'default' }) => {
  const dispatch = useDispatch();
  const history = createBrowserHistory();
  const classes = useStyles({ variant });
  const onFinish = async values => {
    console.log('Success:', values);
    try {
      let  res = await AuthService.onRegister({
        userName: values.username,
        password: values.password,
        fullName: values.fullname,
        email: values.email,
        phoneNumber: values.phonenumber,
      });
      console.log(res);
      history.replace({ pathname: rootPath.signin});
      history.go({ pathname: rootPath.signin });
      await message.success('Đăng kí thành công');
    } catch (error){
      console.log(error.response);
      message.error(error.response.data.message);
    }
  };
  let regex = '(([a-z A-Z 1-9 @$!%*#?&])(?=.*[A-Z][a-z])(?=.*\d)(?=.*[@$!%*#?&])).{7,}';
  const validatePassword = (rule, value, callback) => {
    if(!value.match(regex)) {
      return callback(true)
    }
    callback()
  };
  return (
    <AuthWrapper variant={wrapperVariant}>
      {variant === 'default' ? (
        <Box className={classes.authThumb}>
          <CmtImage src={'/images/auth/sign-up-img.png'} />
        </Box>
      ) : null}
      <Box className={classes.authContent}>
        <h2 style={{ fontSize: '25px', marginBottom: '25px' }}>Create an account</h2>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          id="signup-form"
          autoComplete="off">
          <Form name="normal_login" className="login-form" initialValues={{ remember: true }}  onFinish={onFinish}>
            <Form.Item
              style={{ marginBottom: '25px' }}
              name="fullname"
              rules={[{ required: true, message: 'Please input your Email!' }]}>
              <Input type="text" placeholder="Full Name" style={{ padding: '10px' }} />
            </Form.Item>
            <Form.Item
              style={{ marginBottom: '25px' }}
              name="username"
              rules={[{ required: true, message: 'Please input your Username!' }]}>
              <Input placeholder="Username" style={{ padding: '10px' }} />
            </Form.Item>
            <Form.Item
              style={{ marginBottom: '25px' }}
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập số hộ chiếu',
                },
                () => ({
                  validator(_, value) {
                    const PASSPORT_REG_EX =/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{6,32}$/.test(value);
                    debugger;
                    if (!PASSPORT_REG_EX && value) {
                      return Promise.reject(new Error('Vui lòng nhập chính xác số giấy tờ tuỳ thân'));
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
             >
              <Input type="password" placeholder="Password" style={{ padding: '10px' }} />
            </Form.Item>
            <Form.Item
              style={{ marginBottom: '25px' }}
              name="email"
              rules={[{ required: true, message: 'Please input your Email!' }]}>
              <Input type="text" placeholder="Email" style={{ padding: '10px' }} />
            </Form.Item>
            <Form.Item
              style={{ marginBottom: '25px' }}
              name="phonenumber"
              rules={[{ required: true, message: 'Please input your phone number!' }]}>
              <Input type="text" placeholder="Phone Number" style={{ padding: '10px' }} />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Login
              </Button>
            </Form.Item>
          </Form>
        </Form>
        <Box mb={3}>{dispatch(AuhMethods[method].getSocialMediaIcons())}</Box>
        <Typography style={{ color: 'black' }} className={classes.textAcc}>
          Have an account?
          <NavLink to="/signin">Sign In</NavLink>
        </Typography>
      </Box>
    </AuthWrapper>
  );
};

export default SignUp;
