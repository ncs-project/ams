import React from 'react';
import Button from '@material-ui/core/Button';
import AuthService from 'services/auth/AuthService';
import { rootPath } from 'helpers/buildUrl';
import { Col,  Row, Form, Input, Checkbox } from 'antd';
import { message } from 'antd';
import { createBrowserHistory } from 'history';
import userLogin from '../../../images/user.png';
import passwordIcon from '../../../images/locked-house.png';
import styles from './index.module.scss';

const SignIn = () => {
  const history = createBrowserHistory();
  const onFinish = async values => {
    console.log('Success:', values);
    try {
      let res = await AuthService.onLogin({
        username: values.username,
        password: values.password,
      });
      var infoUser =  JSON.parse(res.config.data);
      localStorage.setItem('userName', infoUser.username);
      localStorage.setItem('tokenUser', res.data.access_token);
      history.replace({ pathname: `${rootPath.home}/upload`});
      history.go({ pathname: `${rootPath.home}/upload`});
      await message.success('Đăng nhập thành công');
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div id="login-form">
      <Row style={{ height: '100%', background: 'linear-gradient(#9ED7ED, #4A87F7)' }}>
      <Col xs={24} md={10} lg={8}>
          <div className="login-container">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              id="login-form"
              autoComplete="off">
              <Row align="middle" style={{ justifyContent: 'center' }}>
                <h1> Welcome Back </h1>
              </Row>
              <Row align="start">
                <h2 className={styles.loginTitle}>Login</h2>
                <div className={styles.signUp}> <div>Not a member?</div> <b> <a href="/signup"> Sign up</a> </b> </div>
              </Row>
              <Form name="normal_login" className="login-form" initialValues={{ remember: true }} onFinish={onFinish}>
                <Form.Item name="username" rules={[{ required: true, message: 'Please input your Username!' }]}>
                  <Input
                    prefix={
                      <>
                        {' '}
                        <img className="icon-login" src={userLogin} /> |{' '}
                      </>
                    }
                    placeholder="Username"
                  />
                </Form.Item>
                <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }]}>
                  <Input
                    prefix={
                      <>
                        {' '}
                        <img className="icon-login" src={passwordIcon} /> |{' '}
                      </>
                    }
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>
                <Form.Item>
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Remember me</Checkbox>
                  </Form.Item>
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </Form>
          </div>
        </Col>
        <Col xs={24} md={14} lg={16} classsName={styles.title}>
          <div className={styles.loginRight}>
            <h2>Donec in dapibus augue sed nisi nunc suscipit eget enim sit amet</h2>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default SignIn;
