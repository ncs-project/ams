import React from 'react';
import styles from './index.module.scss';
import { Layout } from 'antd';
import { useLocation } from 'react-router';
import MenuSearch from './MenuSearch.js';
import MenuHeader from './MenuHeader.js';

const { Header, Content, Footer } = Layout;

const VerticalDefault = ({ children }) => {
  let location = useLocation();
  let pathname = location.pathname.split('/');
  return (
    <Layout style={{ overflow: 'hidden', position: 'relative' }}>
      <Layout className="site-layout" style={{ width: '100%', background: '#fff' }}>
        <Header className={styles.headerContent}>
          {
            pathname[1] == 'search' ? <MenuSearch keyword={pathname[2]} /> : <MenuHeader/>
          }
        </Header>
        <Content className="site-layout-background">
          <div
            style={{
              width: '100%',
              minHeight: '100vh'
            }}>
            {children}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}>
         All Rights Reserved. Developed By NCS
        </Footer>
      </Layout>
    </Layout>
  );
};

export default VerticalDefault;
