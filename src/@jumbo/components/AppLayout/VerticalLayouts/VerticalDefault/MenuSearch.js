import React, { useState } from 'react';
import styles from './index.module.scss';
import './index.scss';
import { Menu, Avatar, Dropdown, Button, Input, Upload, Tooltip } from 'antd';
import { rootPath } from 'helpers/buildUrl';
import { LoginOutlined, SearchOutlined, MenuOutlined, UploadOutlined, HomeOutlined } from '@ant-design/icons';
import { useParams, useHistory } from 'react-router';
import ModalUpload from 'routes/Pages/Home/UploadContent/ModalUpload';
import avatar from '../../../../images/avatar.gif';
import axios from 'axios';
import {baseURL} from 'services/ConfigService/APIServiceAMS';

export default function MenuHeader({ keyword }) {
  const token = localStorage.getItem('tokenUser');
  const userName = localStorage.getItem('userName');
  const [open, setOpen] = useState(false);
  const [key, setKey] = useState(keyword);
  const history = useHistory();
  const [isUpload, setIsUpLoad] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [textUpload, setTextUpload] = useState('Choose File');
  const [textUploadPopUp, setTextUploadPopUp] = useState('Choose File');
  const showDrawer = () => {
    setOpen(true);
  };
  const handleDrag = () => {
    showModal();
  };
  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const handleSignOut = () => {
    localStorage.removeItem('tokenUser');
    history.replace({ pathname: `${rootPath.home}/upload` });
  };
  const props = {
    beforeUpload: async fileSelect => {
      setIsUpLoad(true);
      setFileList([...fileList, fileSelect]);
      const formData = new FormData();
      formData.append('file', fileSelect);
      const config = {
        onUploadProgress: progressEvent => {
          const { loaded, total } = progressEvent;
          let percent = Math.floor((loaded * 100) / total);

          if (open) {
            setTextUploadPopUp(`Uploading... ${percent}%`);
          } else {
            setTextUpload(`Uploading... ${percent}%`);
          }
          console.log(`${loaded}kb of ${total}kb | ${percent}%`);
        },
      };
      axios
        .post(`${baseURL}seaweedfs/uploadFile`, formData)
        .then(res => {
          if (res.status === 200) {
            var data = res.data;
            var tagId = data.id ? data.id : data[0].id;
            if (open) {
              setTextUploadPopUp(`Upload success!`);
            } else {
              setTextUpload(`Upload success!`);
            }
            localStorage.setItem('fileData', JSON.stringify(res.data));
            setTimeout(() => goToConfig(tagId), 1000);
          }
         
        })
        .catch(err => {
          if (open) {
            setTextUploadPopUp(`Upload fail`);
          } else {
            setTextUpload(`Upload fail`);
          }
          console.log('error: ', err.message);
        });
    },
    fileList,
  };
  const items = [
    {
      label: 'Upload Sample',
      key: `${rootPath.home}/upload`,
    },
    {
      label: 'Report',
      key: 'report',
    },
  ];
  const handleSearch = e => {
    setKey(e.target.value)
    window.localStorage.setItem('keySearch', e.target.value);
    window.dispatchEvent(new Event('storage'));
  };
  const handleSearchClick = e => {
    history.replace({ pathname: `${rootPath.search}/${key}` });
  };
  const goToConfig = (tagId) => {
    history.replace({pathname: `${rootPath.configure}/${tagId}`})
    // history.go(`${rootPath.configure}/${tagId}`)
  }
  const menuProps = (
    <Menu>
      <Menu.Item key="1" onClick={handleSignOut}>
        <LoginOutlined /> Sign out
      </Menu.Item>
    </Menu>
  );
  const onclickMenu = e => {
    history.replace({ pathname: e.key });
  };
  return (
    <>
      <div className={styles.searchWrapper}>
        <Button className={styles.menuButton} type="text" onClick={showDrawer}>
          <MenuOutlined />
        </Button>
        <div className={styles.logoSearch} type="text" onClick={()=>{ history.replace({pathname: `${rootPath.home}/upload`})  }}>
          <HomeOutlined />
        </div>
        <div className={styles.inputContainer}>
          <Input placeholder="URL, IP address, domain, or file hash" defaultValue={keyword} onChange={handleSearch} />
        </div>
        <div className={styles.iconButton}>
          <div className={styles.grey}>
            <Tooltip title="Search">
              <SearchOutlined style={{ cursor: 'pointer' }} onClick={handleSearchClick} />
            </Tooltip>
          </div>
          <div className={styles.uploadButton}>
          <Upload {...props} showUploadList={false}>
            <Tooltip title="Upload file">
              <UploadOutlined style={{ cursor: 'pointer' }} onClick={showModal}/>
            </Tooltip>
            </Upload>
          </div>
        </div>

        {/* <Drawer title="Menu header" placement="right" onClose={onClose} open={open}>
            <Menu onClick={onclickMenu}>
              <Menu.Item key={`${rootPath.home}/upload`}>Upload Sample</Menu.Item>
              <Menu.Item key="api">API calls</Menu.Item>
              <Menu.Item key="setting">Setting</Menu.Item>
            </Menu>
          </Drawer> */}

        <div style={{ float: 'right', display: 'flex', alignItems: 'center', marginRight: '10px', marginLeft: '15px' }}>
          {token && userName ? (
            <>
              <div className={styles.loginWrapper}>
                <div className={styles.avatarContent}>
                  <span className="avatar-name">{userName}</span>
                  <Dropdown overlay={menuProps} trigger={['click']} placement="bottomRight" arrow>
                    <Avatar className={styles.avatar} src={avatar} size="large"></Avatar>
                  </Dropdown>
                </div>
              </div>
            </>
          ) : (
            <Menu className={styles.menuRight} style={{ border: 'none' }} onClick={onclickMenu}>
              <Menu.Item key={rootPath.signin}>Sign in</Menu.Item>
              <Menu.Item key={rootPath.signup} className={styles.signup}>
                Sign up
              </Menu.Item>
            </Menu>
          )}
        </div>
      </div>
      <ModalUpload props={props} textUploadPopUp={textUploadPopUp} open={open} handleCancel={handleCancel} />
    </>
  );
}
