import React, { useEffect, useState } from 'react';
import styles from './index.module.scss';
import { Layout, Menu, Avatar, Dropdown, Button, Drawer } from 'antd';
import { rootPath } from 'helpers/buildUrl';
import { LoginOutlined, MenuOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router';
import avatar from '../../../../images/avatar.gif';

export default function MenuHeader() {
  const token = localStorage.getItem('tokenUser');
  const userName = localStorage.getItem('userName');
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const showDrawer = () => {
    setOpen(true);
  };
  const handleSignOut = () => {
    localStorage.removeItem('tokenUser');
    history.replace({ pathname: `${rootPath.home}/upload` });
  };
  const items = [
    {
      label: 'Upload Sample',
      key: `${rootPath.home}/upload`,
    },
    // {
    //   label: 'Report',
    //   key: 'report',
    // },
    // {
    //   label: 'Setting',
    //   key: 'setting',
    // },
  ];
  const menuProps = (
    <Menu>
      <Menu.Item key="1" onClick={handleSignOut}>
        <LoginOutlined /> Sign out
      </Menu.Item>
    </Menu>
  );
  const onclickMenu = e => {
    history.replace({ pathname: e.key});
  };
  return (
    <>
      <Button className={styles.menuButton} type="text" onClick={showDrawer}>
        <MenuOutlined />
      </Button>
      {/* <Drawer title="Menu header" placement="right" onClose={onClose} open={open}>
            <Menu onClick={onclickMenu}>
              <Menu.Item key={`${rootPath.home}/upload`}>Upload Sample</Menu.Item>
              <Menu.Item key="api">API calls</Menu.Item>
              <Menu.Item key="setting">Setting</Menu.Item>
            </Menu>
          </Drawer> */}
      <Menu className={styles.menuHeader} mode="horizontal" items={items} onClick={onclickMenu} />
      <div style={{ float: 'right', display: 'flex', alignItems: 'center', marginRight: "10px" }}>
        {token && userName ? (
          <>
            <span className={styles.helloUser}>Xin chào</span>
            <div className={styles.loginWrapper}>
              <div className={styles.avatarContent}>
                <span className="avatar-name">{userName}</span>
                <Dropdown overlay={menuProps} trigger={['click']} placement="bottomRight" arrow>
                  <Avatar className={styles.avatar} src={avatar} size="large"></Avatar>
                </Dropdown>
              </div>
            </div>
          </>
        ) : (
          <Menu className={styles.menuRight} style={{ border: 'none' }} onClick={onclickMenu}>
            <Menu.Item key={rootPath.signin}>Sign in</Menu.Item>
            <Menu.Item key={rootPath.signup} className={styles.signup}>
              Sign up
            </Menu.Item>
          </Menu>
        )}
      </div>
    </>
  );
}
