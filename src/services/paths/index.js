export const servicePaths = {
  //account 
  getAllAcount: '/admin/getAll/user',
  createAccount: '/register',
  getAccountById: '/users/',
  updateAccountById: '/users/',
  deleteAccount: '/users/username/',
  //roles 
  getAllRoles : '/roles',
  setRole : '/users/',
  //users 
  getAllUser: '/users',

  //employee
  updateEmployee: '/employees/',

  //user
  userLogin: '/user/signin',
  userRegister: '/user/register',
  updateUser: '/users/',
  getUserByUsername: '/users/username/',

  //File
  getFile: '/testparsejson1',
  getSampleById: '/samples/',
  updateArchById: '/sample/updateArch/',
  uploadFile: '/uploadFile',
  getCoreLogApi: '/es/getCoreLogAPIs/',
  getCoreLogBasic: '/es/getCoreLogBasic/'
};

