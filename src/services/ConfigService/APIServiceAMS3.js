import axios from 'axios';

export const baseURL = "http://192.168.14.29:9004";
const APIServiceAMS2 = (configuration = {}) => {

    const token = localStorage.getItem("tokenUser");
    console.log("token", token);
    let headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': `token ${token}`
    };
    
    const axiosInstance = axios.create({
        baseURL,
        timeout: 10000,
        headers,
        ...configuration,
    });

    axiosInstance.interceptors.request.use(
        config => {
            return config;
        },
        error => Promise.reject(error),
    );

    axiosInstance.interceptors.response.use(
        response => response,
        error => {
            return Promise.reject(error);
        },
    );

    return axiosInstance;

}
export default APIServiceAMS2;