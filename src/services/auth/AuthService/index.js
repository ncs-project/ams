import { CONFIGURATION } from 'Configuration';
import ApiService from 'services/ConfigService/APIService';
import { servicePaths } from 'services/paths';

const AuthService = {
  
  onLogin: data => ApiService().post(servicePaths.userLogin, data),
  onLogout: data => ApiService().post(servicePaths.userLogout, data),
  onRegister: (data) => ApiService().post(servicePaths.userRegister , data),

};
export default AuthService;
