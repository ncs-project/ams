import ApiService from "services/ConfigService/APIService";
import { servicePaths } from "services/paths";

const AccountService = {
    getAllAcount: () => ApiService().get(servicePaths.getAllAcount),
    createAccount: (data) => ApiService().post(servicePaths.createAccount,data),
    getAccountById: (param) => ApiService().get(servicePaths.getAccountById + param),
    updateAccountById:(params,data) => ApiService().post(servicePaths.updateAccountById + params , data),
    deleteAccount: (params) => ApiService().delete(servicePaths.deleteAccount + params),
    // getAccountById: (param) => ApiService().get(servicePaths.getAccountById + param),
};

export default AccountService;