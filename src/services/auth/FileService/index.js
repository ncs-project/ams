import APIServiceAMS from "services/ConfigService/APIServiceAMS";
import APIServiceAMS2 from "services/ConfigService/APIServiceAMS2";
import { servicePaths } from "services/paths";

const UserService = {
    getFile: () => APIServiceAMS().get(servicePaths.getFile),
    getSampleById: (params) => APIServiceAMS().get(servicePaths.getSampleById + params),
    updateArchById: (params, data) => APIServiceAMS().post(servicePaths.updateArchById + params, data),
    uploadFile: (data) => APIServiceAMS().post(servicePaths.uploadFile, data),
    getCoreLogApi: (params) => APIServiceAMS2().get('/'+ params),
    getCoreLogBasic: (params) => APIServiceAMS2().get('/'+ params),
}

export default UserService;