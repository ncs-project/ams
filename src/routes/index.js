import React from 'react';
import { rootPath } from 'helpers/buildUrl';
import { useSelector } from 'react-redux';
import {  Route , Switch } from 'react-router';
import Error404 from './Pages/404';
import SignIn from '@jumbo/components/Common/authComponents/SignIn';
import SignUp from '@jumbo/components/Common/authComponents/SignUp';
import Home from '../routes/Pages/Home';
import Configure from '../routes/Pages/Configure';
import File from '../routes/Pages/File';
import Search from '../routes/Pages/Search';
import ExportFIle from '../routes/Pages/ExportFIle';
import Pending from '../routes/Pages/Pending';
import { useLocation, Redirect } from 'react-router-dom';
const Routes = props => {
  let location = useLocation();
  useSelector(({ auth }) => auth);
  if (location.pathname === '' || location.pathname === '/' || location.pathname === rootPath.prefix) {
    return <Redirect to={`${rootPath.home}/upload`} />;
  }
  //  else if (!token && location.pathname !== rootPath.signin && location.pathname !== rootPath.signup) {
  //   return <Redirect to={`${rootPath.home}/upload}`} />;
  // } 
  // else if (token && location.pathname === rootPath.signin) {
  //   return <Redirect to={`${rootPath.signin}`} />;
  // }
  return (
    <React.Fragment>
      <Switch>
        <Route path={rootPath.signin} component={SignIn} />
        <Route path={rootPath.signup} component={SignUp} />
        <Route path={rootPath.home} component={Home} />
        <Route path={rootPath.configure} component={Configure} />
        <Route path={rootPath.file} component={File} />
        <Route path={rootPath.search} component={Search} />
        <Route path={rootPath.export} component={ExportFIle} />
        <Route path={rootPath.pending} component={Pending} />
        <Route component={Error404} />
      </Switch>
    </React.Fragment>
  );
};

export default Routes;
