import React from 'react';
import styled from './Export.module.scss';
import { CloudDownloadOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';

import ChooBox from './Checkbox/ChooBox';

import BtnDownload from './Checkbox/BtnDownload';
import ChooBox1 from './Checkbox/ChooBox1';

const Export = () => {
  return (
    <div className={styled.container}>
      <span className={styled.container_icon}>
        <CloudDownloadOutlined className={styled.container_icon_img} />
        <h4>Export analysis</h4>
      </span>

      <div className={styled.container_select}>
        <header>
          <span>Options</span>
        </header>

        <div className={styled.container_select_box}>
          <span>Select which files you want to include in the export.</span>

          <div className={styled.container_select_box_wrapper}>
            <Row>
              <Col span={12}>
                <ChooBox id={'123'} />
                <ChooBox id={'124'} />
              </Col>
              <Col span={12}>
                <ChooBox id={'125'} />
              </Col>
            </Row>
          </div>
        </div>

        <Row>
          <Col span={24}>
            <div className={styled.container_select_footer}>
              <div className={styled.container_select_footer_tabtable}>
                <span>Chosen analysis nr.3808451 to export</span>
              </div>
              <input type="text" value="   TL_Du_an_AMAS (4).docx" disabled style={{ margin: '10px 10px' }}></input>
            </div>

            <div className={styled.container_select_download}>
              <BtnDownload />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Export;

