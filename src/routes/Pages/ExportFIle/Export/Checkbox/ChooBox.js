import React from 'react';
// import { Checkbox } from 'antd';

import styled from './ChooBox.module.scss';

const ChooBox = ({id}) => {
  return (
    <>
      <div className={styled.container}>
        <div className={styled.container_wrapper}>
          <input type="checkbox" id={id} name="dirs" value="files"></input>
          <label for={id}>file (15 files)</label>
        </div>
      </div>
    </>
  );
};

export default ChooBox;

