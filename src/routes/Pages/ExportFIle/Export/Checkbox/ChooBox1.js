import React from 'react';
import styled from './ChooBox.module.scss';

const ChooBox1 = () => {
  return (
    <div>
      <>
        <div className={styled.container}>
          <div className={styled.container_wrapper}>
            <input type="checkbox" id="export_shorts" name="dirs" value="short"></input>
            <label for="export_file">short (14 files)</label>
          </div>
        </div>
      </>
    </div>
  );
};

export default ChooBox1;

