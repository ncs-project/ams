import React from 'react';
import { Button, Space } from 'antd';
import styled from './BtnDownload.module.scss';
import { DownCircleOutlined } from '@ant-design/icons';

const BtnDownload = () => {
  return (
    <Space
      direction="vertical"
      style={{
        width: '100%',
      }}>
      <Button className={styled.button} type="primary">
        <DownCircleOutlined className={styled.button_down} />
        Download
      </Button>
    </Space>
  );
};

export default BtnDownload;
