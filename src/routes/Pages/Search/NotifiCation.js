import React from 'react';
// import { Button, Result } from 'antd';
import styled from './NotifiCation.module.scss';
import img1 from '../../../assset/images/image.svg';
import ButtonClick from './ButtonClick';

const NotifiCation = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_img}>
        <img src={img1}></img>
      </div>
      <div className={styled.container_title}>
        <h3>No matches found</h3>
      </div>
      {/* <div className={styled.container_message}>
        <span> Are you looking for advanced malware searching capabilities? VT Intelligence can help, </span>
        <a href="">learn more</a>
      </div> */}
      {/* <div className={styled.container_actions}>
        <ButtonClick/>
      </div> */}
    </div>
  );
};

export default NotifiCation;

