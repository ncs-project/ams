import React from 'react';
import { Button, Space } from 'antd';
import styled from './ButtonClick.module.scss'

const App = () => (
  <Space wrap>
    <Button className={styled.btn} type="primary">Try a new search</Button>
  </Space>
);
export default App;
