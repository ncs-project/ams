import React, { useEffect, useState } from 'react';
import styled from './Search.module.scss';
import { useParams } from 'react-router';
import axios from 'axios';
import NotifiCation from './NotifiCation';
import { rootPath } from 'helpers/buildUrl';
import {baseURL} from 'services/ConfigService/APIServiceAMS3';
const Search = () => {
  const [titleExpand, setTitleExpand] = useState(false);
  const { keyword } = useParams();
  const [key, setKey] = useState(keyword);
  const [postList, setPostList] = useState([]);
  const [notFound, setNotFound] = useState(false);
  window.addEventListener('storage', () => {
    setKey(localStorage.getItem('keySearch'));
    console.log(key);
    // ...
})
  //call api
  async function getSearch() {
    try {
      const response = await axios({
        method: 'GET',
        url: `${baseURL}/es/samples/md5/${key}`,
      });
      if(response.data.length === 0){
        setNotFound(true);
      }
      else{
        setPostList(response.data);
        setNotFound(false);
      }
    } catch (error) {
      
      console.log(error);
    }
  }

  useEffect(() => {
    getSearch();
  }, [key]);

  return (
    <>
      {notFound? (
        <NotifiCation />
      ) : (
        postList.map((data, key) => {
          return (
            <div key={data.id} className={styled.comment_wrapper}>
              <header className={styled.comment_wrapper_header}>
                <div className={styled.comment_wrapper_header_details}>
                  <p className={styled.comment_wrapper_header_details_metaname}>
                    <span className={styled.comment_wrapper_header_details_metaname_date}>
                    </span>
                    <span className={styled.comment_wrapper_header_details_metaname_id}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        viewBox="0 0 24 24">
                        <path d="M17.3,3.6H4.8v17.5h14.4V5.5L17.3,3.6z M17.2,4.9l0.7,0.7h-0.7V4.9z M5.8,20.1V4.6h10.4v2h2v13.5H5.8z"></path>
                      </svg>
                      <a href={`${rootPath.file}/${data.id}/behavior`}>Sample Id # {data.id}</a>
                    </span>
                  </p>
                </div>
              </header>

              <div class={`${styled.body} ${titleExpand ? styled.expand : ''}`}>
                <p class={styled.body_title}>
                  <p class={styled.body_title}>
                    {/* id:"{data.id}" */}
                    {/* <br />
                    filesSize:{data.fileSize} */}
                    <br />
                    magic:"{data.magic}"
                    <br />
                    md5:"{data.md5}"
                    <br />
                    sha1:"{data.sha1}"
                    <br />
                    sha256:"{data.sha256}"
                  </p>
                </p>
                {/* <div className={styled.toggle}>
                  <a className={styled.btn_show} onClick={() => setTitleExpand(!titleExpand)}>
                    {titleExpand ? 'Show more' : 'Show less'}
                  </a>
                </div> */}
              </div>
            </div>
          );
        })
      )}
    </>
  );
};

export default Search;

// tạo css expand

