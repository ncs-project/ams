import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '@jumbo/components/PageComponents/PageLoader';

const Search = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  console.log('requestedUrl', requestedUrl);
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route path={`${requestedUrl}/:keyword`} component={lazy(() => import('./Search'))} />
        <Route component={lazy(() => import('../404'))} />
      </Switch>
    </Suspense>
  );
};
export default Search;
