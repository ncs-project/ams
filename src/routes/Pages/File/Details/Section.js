import { Collapse, Table } from 'antd';
import React from 'react';
import styled from '../commonText.module.scss';
const DetailsText = ( {detail} ) => {
  const { Panel } = Collapse;
  const sections = detail.sections;
  const dataSource = [];
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Entropy',
      dataIndex: 'entropy',
      key: 'entropy',
    },
    {
      title: 'Md5',
      dataIndex: 'md5',
      key: 'md5',
    },
    {
      title: 'MawSize',
      dataIndex: 'rawSize',
      key: 'rawSize',
    },
    {
      title: 'VirtualAddress',
      dataIndex: 'virtualAddress',
      key: 'virtualAddress',
    },
    {
      title: 'VirtualSize',
      dataIndex: 'virtualSize',
      key: 'virtualSize',
    },
  ]
  if(sections){
    Object.keys(sections).forEach(key => {
      console.log(key, sections[key]);
      let data =  {
        key: key,
        name: key,
        entropy: sections[key].entropy,
        md5: sections[key].md5,
        rawSize: sections[key].rawSize,
        virtualAddress: sections[key].virtualAddress,
        virtualSize: sections[key].virtualSize,
      };
      dataSource.push(data);
    })
  }
  // const peId = detail.peId;
  return (
    <div className={styled.container_description_wrapper_details} style={{paddingTop: "10px"}}>
      <div className={styled.container_description_wrapper_details_content}>
        {/* li - item */}
        <div className={styled.container_description_wrapper_details_content_detections}>
          {/* detail */}
          <div className={styled.container_description_wrapper_details_content_detections_detail}>
            <div className={styled.container_description_wrapper_details_content_detections_detail_wrapper}>
              <h4>Sections</h4>
              <Table dataSource={dataSource} columns={columns} pagination={false} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailsText;

// li - item

