import React, { useEffect, useState } from 'react';
import styled from '../index.module.scss';
import '../action.scss';
import styles from '../common.module.scss';
import DetailsText from './DetailsText';
import Section from './Section';
import axios from 'axios';
import { Col, Row, Tabs, Button, Tooltip } from 'antd';
import dllSVG from '../../../../assset/images/dll.svg';
import exeSVG from '../../../../assset/images/exe.svg';
import { DownloadOutlined } from '@ant-design/icons';
import { useHistory, useParams } from 'react-router-dom';
import FileService from 'services/auth/FileService';
import { baseURL } from 'services/ConfigService/APIServiceAMS';
const Details = () => {
  const history = useHistory();
  const [detail, setDetail] = useState({});
  const [fileName, setFileName] = useState('');
  const { sampleId } = useParams();
  const [currentFile, setCurrenFile] = useState('');
  const intervals = [
    { label: 'year', seconds: 31536000 },
    { label: 'month', seconds: 2592000 },
    { label: 'day', seconds: 86400 },
    { label: 'hour', seconds: 3600 },
    { label: 'minute', seconds: 60 },
    { label: 'second', seconds: 1 },
  ];
  const chooseImgFile = paramfile => {
    let fileType = paramfile.split('.');
    fileType = fileType[fileType.length - 1].toLowerCase();
    if (fileType === 'dll') {
      return <img src={dllSVG} alt="img-file" />;
    } else if (fileType === 'exe') {
      return <img src={exeSVG} alt="img-file" />;
    }
  };
  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      hour = d.getHours(),
      minute = d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    var date = [day, month, year].join('/');
    var time = [hour, minute].join(':');

    return (
      <>
        {date}
        <span style={{ paddingRight: '5px' }}></span>
        {time}
      </>
    );
  }
  function timeSince(date) {
    var date = new Date(date);
    const seconds = Math.floor((Date.now() - date.getTime()) / 1000);
    const interval = intervals.find(i => i.seconds < seconds);
    const count = Math.floor(seconds / interval.seconds);
    return `${count} ${interval.label}${count !== 1 ? 's' : ''} ago`;
  }
  const changeTab = key => {
    history.push(key);
  };
  const downloadFile = () => {
    const formData = new FormData();
      formData.append('basic', 1);
      formData.append('api', 1);
      formData.append('sample', 1);
      const link = document.createElement("a");
    link.target = "_blank";
    link.download = `${sampleId}.zip`;
      axios
      .post(`${baseURL}downloadfile/${sampleId}`, formData, {
        responseType: "blob",
      })
      .then((res) => {
        link.href = URL.createObjectURL(
          new Blob([res.data], { type: "application/zip" })
        );
        link.click();
      });
  };
  const callApiGetLog = async paramFileCoreLogBasic => {
    try {
      let res = await FileService.getCoreLogBasic(paramFileCoreLogBasic);
      debugger;
      setDetail(res.data);
    } catch (error) {
      console.log(error);
    }
  };
  const callApiGetFile = async () => {
    try {
      let res = await FileService.getSampleById(sampleId);
      debugger;
      setCurrenFile(res.data);
      setFileName(res.data.fileName);
      callApiGetLog(res.data.fidCoreLogBasic);
    } catch (error) {
      console.log(error);
    }
  };
  //useEffect
  useEffect(() => {
    callApiGetFile();
  }, []);
  return (
    <div className={styled.container}>
      {/* dau */}
      <Row className={styled.container_content}>
        <div className={styled.wrapper}>
          {/* chart */}
          <Col span={3}>
            <div className={styled.leftChart}></div>
          </Col>

          {/* infomation */}
          <Col span={21}>
            <div className={styled.rightParam}>
              <div className={styled.header}>
                <div className={styled.detections}>
                  <span className={styled.detections_wrapper}>
                    <div className={styled.detections_wrapper_conetent}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        viewBox="0 0 24 24">
                        <path d="M12,21.3c-4.9,0-8.8-4-8.8-8.8c0-4.9,4-8.8,8.8-8.8c4.9,0,8.8,4,8.8,8.8C20.8,17.4,16.9,21.3,12,21.3z M12,4.7   c-4.3,0-7.8,3.5-7.8,7.8c0,4.3,3.5,7.8,7.8,7.8c4.3,0,7.8-3.5,7.8-7.8C19.8,8.2,16.3,4.7,12,4.7z"></path>
                        <path d="M10.4,15.9C10.4,15.9,10.4,15.9,10.4,15.9c-0.2,0-0.3-0.1-0.4-0.2l-2.6-3.3c-0.2-0.2-0.1-0.5,0.1-0.7  c0.2-0.2,0.5-0.1,0.7,0.1l2.2,2.8l5.2-6c0.2-0.2,0.5-0.2,0.7-0.1c0.2,0.2,0.2,0.5,0.1,0.7l-5.6,6.5C10.7,15.9,10.6,15.9,10.4,15.9z"></path>
                      </svg>
                      <p>No security vendors and no sandboxes flagged this file as malicious</p>
                    </div>
                  </span>
                  <Tooltip title="download">
                    {/* <a href={`http://192.168.14.29:9004/es/downloadFile/coreLogBasic/${sampleId}`}> */}
                    <Button icon={<DownloadOutlined />} onClick={downloadFile}>
                      Download
                    </Button>
                    {/* </a> */}
                  </Tooltip>
                </div>
              </div>

              <div className={styled.body}>
                <div className={styled.row}>
                  <div className={styled.object_id}>
                    {/* <div className={styled.object_id_file}>
                      f47bfead9abc6eb1f508f2343826aeaded28b9d6884a57ef7297f654f1b59f72
                    </div> */}
                    <div className={styled.object_id_name}>
                      <a href="">{currentFile.fileName}</a>
                    </div>
                  </div>

                  <div className={styled.field}>
                    <div className={styled.field_title}>
                      <a href="">{currentFile.fileSize}</a>
                    </div>
                    <div className={styled.field_text}>Size</div>
                  </div>

                  <div className={styled.field}>
                    <div className={styled.field_title}>{currentFile ? formatDate(currentFile.completeOn) : ''}</div>
                    <span className={styled.field_text}> {currentFile ? timeSince(currentFile.completeOn) : ''} </span>
                  </div>

                  <div className={styled.images}>{fileName ? chooseImgFile(fileName) : ''}</div>
                </div>
              </div>
            </div>
          </Col>
        </div>
      </Row>
      {/* giua */}
      <Col span={24}>
        <Tabs
          className={styled.tabs}
          id="tab-container"
          defaultActiveKey="details"
          onChange={changeTab}
          items={[
            {
              label: 'Detail',
              key: 'details',
            },
            {
              label: 'Behavior',
              key: 'behavior',
            },
          ]}
        />
      </Col>

      {/* than */}
      <div className={styled.container_description}>
        <div className={styled.container_description_wrapper}>
          {/* 1 */}

          <div className={styled.container_description_wrapper_details}>
            <div className={styled.container_description_wrapper_details_content}>
              {/* 1 _ basic */}
              <div className={styles.container_description_wrapper_header}>
                <h3 className={styles.container_description_wrapper_header_title}>Basic Properties</h3>
                <div className={styles.container_description_wrapper_header_button}>
                  <a className={styles.container_description_wrapper_header_button_text} href="">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" fill="currentColor">
                      <path stroke-miterlimit="10" d="M18.9,3.5"></path>
                      <path
                        d="M12,21.1c-4.9,0-8.8-4-8.8-8.8c0-4.9,4-8.8,8.8-8.8c4.9,0,8.8,4,8.8,8.8C20.8,17.1,16.9,21.1,12,21.1z M12,4.5
          c-4.3,0-7.8,3.5-7.8,7.8c0,4.3,3.5,7.8,7.8,7.8c4.3,0,7.8-3.5,7.8-7.8C19.8,8,16.3,4.5,12,4.5z"></path>
                      <path d="M11.4,7.6h1.3v2l-1.3,1V7.6z M12.6,11v5.9h-1.3V12L12.6,11z"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <DetailsText detail={detail} />
              <div className={styles.container_description_wrapper_header}>
                <h3 className={styles.container_description_wrapper_header_title}>Portable Executable Info</h3>
                <div className={styles.container_description_wrapper_header_button}>
                  <a className={styles.container_description_wrapper_header_button_text} href="">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" fill="currentColor">
                      <path stroke-miterlimit="10" d="M18.9,3.5"></path>
                      <path
                        d="M12,21.1c-4.9,0-8.8-4-8.8-8.8c0-4.9,4-8.8,8.8-8.8c4.9,0,8.8,4,8.8,8.8C20.8,17.1,16.9,21.1,12,21.1z M12,4.5
          c-4.3,0-7.8,3.5-7.8,7.8c0,4.3,3.5,7.8,7.8,7.8c4.3,0,7.8-3.5,7.8-7.8C19.8,8,16.3,4.5,12,4.5z"></path>
                      <path d="M11.4,7.6h1.3v2l-1.3,1V7.6z M12.6,11v5.9h-1.3V12L12.6,11z"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <Section detail={detail} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
