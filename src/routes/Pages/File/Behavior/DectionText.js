import { CaretRightOutlined, MinusOutlined, PlusOutlined } from '@ant-design/icons';
import { Collapse } from 'antd';
import React from 'react';
import styled from '../commonText.module.scss';
import Test from './Log.json';
const DetailsText = () => {
  console.log(Test);
  const { Panel } = Collapse;
  const sections = Test.sections;
  const peId = Test.peId;
  return (
    <div className={styled.container_description_wrapper_details}>
      <div className={styled.container_description_wrapper_details_content}>
        {/* li - item */}
        <div className={styled.container_description_wrapper_details_content_detections}>
          {/* detail */}
          <div className={styled.container_description_wrapper_details_content_detections_detail}>
            <div className={styled.container_description_wrapper_details_content_detections_detail_wrapper}>
              <a className={styled.key}>Sections</a>
              <a className={styled.code}>
                <Collapse ghost expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                  <Panel header="Sections" key="1">
                    {Object.keys(sections).map(i => {
                      console.log(i);
                      let obj = sections[i];
                      return (
                        <Collapse key = {i} ghost expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                          <Panel header={i} key="2">
                            <p>Entropy : {obj.entropy}</p>
                            <p>MD5 : {obj.md5}</p>
                            <p>Raw Size : {obj.rawSize}</p>
                            <p>Virtual Address : {obj.virtualAddress}</p>
                            <p>Virtual Size : {obj.md5}</p>
                          </Panel>
                        </Collapse>
                      );
                    })}
                  </Panel>
                </Collapse>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailsText;

// li - item

