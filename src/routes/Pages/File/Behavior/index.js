import styled from '../index.module.scss';
import '../action.scss';
import { Col, Row, Tabs, Collapse, Tooltip, Button } from 'antd';
import { MinusOutlined, PlusOutlined, DownloadOutlined} from '@ant-design/icons';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import dllSVG from '../../../../assset/images/dll.svg';
import exeSVG from '../../../../assset/images/exe.svg';
import { useHistory, useParams } from 'react-router-dom';
import FileService from 'services/auth/FileService';
import {baseURL} from 'services/ConfigService/APIServiceAMS';
const Behavior = () => {
  const { Panel } = Collapse;
  const history = useHistory();
  const [detection, setDetection] = useState({});
  const [currentFile, setCurrenFile] = useState('');
  const [date, setDate] = useState('');
  const [fileName, setFileName] = useState('');
  const { sampleId } = useParams();
  const checkObj = param => {
    if (param) {
      if (typeof param === 'object') {
        if (Object.keys(param)?.length > 0) {
          return (
            <>
              {Object.keys(param)?.map((element, i) => {
                return (
                  <Collapse
                    key={i}
                    ghost
                    expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                    <Panel header={i} key="2">
                      { Object.values(param)[i]}
                    </Panel>
                  </Collapse>
                );
              })}
            </>
          );
        }
      } else {
        if (Object.keys(param)?.length === 0) {
          return param
        }
      }
    }
  };
  const callApiGetLog = async (paramFidCoreLogApi) => {
    try {
      let res = await FileService.getCoreLogApi(paramFidCoreLogApi);
      setDetection(res.data);
    } catch (error) {
      console.log(error);
    }
  }
  const callApiGetFile = async () => {
    try {
      let res = await FileService.getSampleById(sampleId);
      setCurrenFile(res.data);
      setDate(res.data.completeOn);
      setFileName(res.data.fileName);
      callApiGetLog(res.data.fidCoreLogApi);
    } catch (error) {
      console.log(error);
    }
  }
  
  const changeTab = key => {
    history.push(key);
  };
  const intervals = [
    { label: 'year', seconds: 31536000 },
    { label: 'month', seconds: 2592000 },
    { label: 'day', seconds: 86400 },
    { label: 'hour', seconds: 3600 },
    { label: 'minute', seconds: 60 },
    { label: 'second', seconds: 1 }
  ];
  const chooseImgFile = (paramfile) => {

    let fileType = paramfile.split('.');
    fileType = fileType[fileType.length - 1].toLowerCase();
    if(fileType === 'dll'){
      return <img src={dllSVG} alt="img-file" />;
    }
    else if(fileType === 'exe'){
      return <img src={exeSVG} alt="img-file" />;
    }
  }
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minute = d.getMinutes();
    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
        if (hour < 10) hour = '0' + hour;
        if (minute < 10) minute = '0' + minute
    var date =  [ day, month, year].join('/');
    var time =  [hour, minute].join(':');
    return (
      <>
      
       { date }
       <span style={{ paddingRight: "5px" }}></span>
       { time }
      </>
    )
}
  function timeSince(date) {
    var date = new Date(date);
    const seconds = Math.floor((Date.now() - date.getTime()) / 1000);
    const interval = intervals.find(i => i.seconds < seconds);
    const count = Math.floor(seconds / interval.seconds);
    return `${count} ${interval.label}${count !== 1 ? 's' : ''} ago`;
  }
  const downloadFile = () => {
    const formData = new FormData();
      formData.append('basic', 1);
      formData.append('api', 1);
      formData.append('sample', 1);
      const link = document.createElement("a");
    link.target = "_blank";
    link.download = `${sampleId}.zip`;
      axios
      .post(`${baseURL}downloadfile/${sampleId}`, formData, {
        responseType: "blob",
      })
      .then((res) => {
        link.href = URL.createObjectURL(
          new Blob([res.data], { type: "application/zip" })
        );
        link.click();
      });
  };
    //useEffect
    useEffect(() => {
      callApiGetFile();
  },[]);
  return (
    <div className={styled.container}>
      {/* dau */}
      <Row className={styled.container_content}>
        <div className={styled.wrapper}>
          {/* chart */}
          <Col span={3}>
            <div className={styled.leftChart}></div>
          </Col>

          {/* infomation */}
          <Col span={21}>
            <div className={styled.rightParam}>
              <div className={styled.header}>
                <div className={styled.detections}>
                  <span className={styled.detections_wrapper}>
                    <div className={styled.detections_wrapper_conetent}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        viewBox="0 0 24 24">
                        <path d="M12,21.3c-4.9,0-8.8-4-8.8-8.8c0-4.9,4-8.8,8.8-8.8c4.9,0,8.8,4,8.8,8.8C20.8,17.4,16.9,21.3,12,21.3z M12,4.7   c-4.3,0-7.8,3.5-7.8,7.8c0,4.3,3.5,7.8,7.8,7.8c4.3,0,7.8-3.5,7.8-7.8C19.8,8.2,16.3,4.7,12,4.7z"></path>
                        <path d="M10.4,15.9C10.4,15.9,10.4,15.9,10.4,15.9c-0.2,0-0.3-0.1-0.4-0.2l-2.6-3.3c-0.2-0.2-0.1-0.5,0.1-0.7  c0.2-0.2,0.5-0.1,0.7,0.1l2.2,2.8l5.2-6c0.2-0.2,0.5-0.2,0.7-0.1c0.2,0.2,0.2,0.5,0.1,0.7l-5.6,6.5C10.7,15.9,10.6,15.9,10.4,15.9z"></path>
                      </svg>
                      <p>No security vendors and no sandboxes flagged this file as malicious</p>
                    </div>
                  </span>
                  <Tooltip title="download">
                  <Button icon={<DownloadOutlined />} onClick={downloadFile}>    
                      Download 
                    </Button>
                  </Tooltip>
                </div>
              </div>
              <div className={styled.body}>
                <div className={styled.row}>
                  <div className={styled.object_id}>
                    <div className={styled.object_id_name}>
                      <a href="">{currentFile.fileName}</a>
                    </div>
                  </div>

                  <div className={styled.field}>
                    <div className={styled.field_title}>
                      <a href="">{currentFile.fileSize}</a>
                    </div>
                    <div className={styled.field_text}>Size</div>
                  </div>

                  <div className={styled.field}>
                    <div className={styled.field_title}>{currentFile ? formatDate(currentFile.completeOn) : ''}</div>
                    <span className={styled.field_text}> {currentFile ? timeSince(currentFile.completeOn) : ''} </span>
                  </div>

                  <div className={styled.images}>
                    {
                      fileName?chooseImgFile (fileName): ""
                    }  
                      {/* <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        viewBox="0 0 560 554.1">
                        <g>
                          <path
                            d="M90.3,340.7h21.1c12.6,0,25.2,0.9,32.1,2.6c20.5,4,33.4,17.2,33.4,44.5c0,24.6-7,41.8-28.3,49.8
          c-11.1,4.2-25.8,4.3-40.6,4.3H90.3V340.7z M118.9,423.5c6.9,0,14.2-0.6,19.5-2.7c12.1-4.1,16.6-14.7,16.6-31.3
          c0-11.5-3.6-21.3-11.2-25.8c-5.4-3.4-14.2-4.3-23.5-4.3h-8.9v64.2h7.5V423.5z"></path>
                          <path
                            d="M232,444c-29.4,0-47.2-17.8-47.2-50.8c0-36.4,17.5-54.7,50.1-54.7c31,0,47.1,17.1,47.1,49.9C281.9,425.1,263.8,444,232,444
          z M233.8,355.8c-18.1,0-27.6,11.1-27.6,34.3c0,29.8,12.3,36.4,27.1,36.4c15.7,0,26.8-8.7,26.8-34.9
          C260.2,368.1,254,355.8,233.8,355.8z"></path>
                          <path
                            d="M335.9,444c-29.4,0-46-18.3-46-50.2c0-36.1,18-55.3,49-55.3c14.8,0,24.6,2.7,30.9,5.4l-5.7,16.5
          c-6.6-2.5-14.7-4.2-23.1-4.2c-16.6,0-29.7,6.4-29.7,36.9c0,26.5,12.4,33.3,28.5,33.3c10.3,0,19-3.6,28-8.4l6.3,14.7
          C363.2,439.8,351.5,444,335.9,444z"></path>
                          <path
                            d="M372.4,441.9l34.3-53.1L375,340.5h25.3c12.9,18.9,18.3,27.4,21.9,33.3c3.6-5.8,8.2-14.1,21.1-33.1h23.1l-31.9,48.4
          l35.2,52.8h-25.9c-16.3-24.9-20.7-30.9-24.1-36.4l-23.2,36.4L372.4,441.9L372.4,441.9z"></path>
                        </g>
                        <g>
                          <path d="M465.3,317H181.5V119.8h283.9V317H465.3z M202,296.5h242.9V140.3H202V296.5z"></path>
                          <polygon
                            points="191.7,268.2 132.7,268.2 132.7,71 416.5,71 416.5,126.4 396,126.4 396,91.5 153.2,91.5 153.2,247.7 191.7,247.7
          "></polygon>
                          <g>
                            <g>
                              <circle cx="243.8" cy="180.9" r="11"></circle>
                              <path
                                d="M243.8,192.5c-6.4,0-11.5-5.2-11.5-11.5c0-6.4,5.2-11.5,11.5-11.5c6.4,0,11.5,5.2,11.5,11.5
              C255.4,187.3,250.2,192.5,243.8,192.5z M243.8,170.5c-5.7,0-10.4,4.7-10.4,10.4s4.7,10.4,10.4,10.4s10.4-4.7,10.4-10.4
              S249.6,170.5,243.8,170.5z"></path>
                            </g>
                            <g>
                              <rect x="273.3" y="172.2" width="138.4" height="17.4"></rect>
                              <path d="M412.2,190.2H272.7v-18.5h139.5V190.2z M273.9,189h137.2v-16.2H273.9V189z"></path>
                            </g>
                            <g>
                              <circle cx="243.8" cy="218.4" r="11"></circle>
                              <path
                                d="M243.8,230c-6.4,0-11.5-5.2-11.5-11.5c0-6.4,5.2-11.5,11.5-11.5c6.4,0,11.5,5.2,11.5,11.5
              C255.4,224.8,250.2,230,243.8,230z M243.8,208c-5.7,0-10.4,4.7-10.4,10.4s4.7,10.4,10.4,10.4s10.4-4.7,10.4-10.4
              S249.6,208,243.8,208z"></path>
                            </g>
                            <g>
                              <rect x="273.3" y="209.7" width="138.4" height="17.4"></rect>
                              <path d="M412.2,227.7H272.7v-18.5h139.5V227.7z M273.9,226.5h137.2v-16.2H273.9V226.5z"></path>
                            </g>
                            <g>
                              <circle cx="243.8" cy="255.9" r="11"></circle>
                              <path
                                d="M243.8,267.5c-6.4,0-11.5-5.2-11.5-11.5c0-6.4,5.2-11.5,11.5-11.5c6.4,0,11.5,5.2,11.5,11.5
              C255.4,262.3,250.2,267.5,243.8,267.5z M243.8,245.5c-5.7,0-10.4,4.7-10.4,10.4c0,5.7,4.7,10.4,10.4,10.4s10.4-4.7,10.4-10.4
              C254.2,250.2,249.6,245.5,243.8,245.5z"></path>
                            </g>
                            <g>
                              <rect x="273.3" y="247.2" width="138.4" height="17.4"></rect>
                              <path d="M412.2,265.2H272.7v-18.5h139.5V265.2z M273.9,264.1h137.2v-16.2H273.9V264.1z"></path>
                            </g>
                          </g>
                        </g>
                      </svg> */}
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </div>
      </Row>

      {/* giua */}
      <Col span={24}>
        <Tabs
          className={styled.tabs}
          id="tab-container"
          defaultActiveKey="behavior"
          onChange={changeTab}
          items={[
            {
              label: 'Detail',
              key: 'details',
            },
            {
              label: 'Behavior',
              key: 'behavior',
            }
          ]}
        />
      </Col>

      {/* than */}
      <div className={styled.container_description}>
        <div className={styled.container_description_wrapper}>
          {/* 1 */}

          <div className={styled.container_description_wrapper_details}>
            <div className={styled.container_description_wrapper_details_content}>
              {/* 1 _ basic */}
              {Object.keys(detection).map(i => {
                let arr = detection[i];
         
                return (
                  <Collapse
                    key={i}
                    ghost
                    expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                    <Panel header={i} key="2">
                      {arr.map(function(element, index) { 
                        return (
                          <Collapse
                            key={index}
                            ghost
                            expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                            <Panel header={element.position} key="2">
                              {Object.keys(element).map((i, index) => {
                                var obj = element[i];
                                debugger;
                                return (
                                  <>
                                    {
                                      checkObj(obj) && i !== "position" ? 
                                      <Collapse
                                      key={index}
                                      ghost
                                      expandIcon={isActive => (isActive.isActive ? <MinusOutlined /> : <PlusOutlined />)}>
                                      <Panel header={i} key="2">
                                        {checkObj(obj)}
                                      </Panel>
                                    </Collapse> : null
                                    }
                                  </>
                                );
                              })}
                            </Panel>
                          </Collapse>
                        );
                      })}
                    </Panel>
                  </Collapse>
                );
              })}
              {/* <DectionText /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Behavior;
