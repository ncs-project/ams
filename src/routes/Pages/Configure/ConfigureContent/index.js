import React, { useEffect, useState } from 'react';
import { rootPath } from 'helpers/buildUrl';
import { Select } from 'antd';
import axios from 'axios';
import {
  SettingOutlined,
  CheckOutlined,
  GlobalOutlined,
  InfoCircleOutlined,
  FileOutlined,
  FileZipOutlined,
} from '@ant-design/icons';
import styles from '../configure.module.scss';
import { createBrowserHistory } from 'history';
import { RotatingLines } from 'react-loader-spinner';
import { useParams } from 'react-router';
import {baseURL} from 'services/ConfigService/APIServiceAMS';
import FileService from 'services/auth/FileService';
import { toDate } from 'date-fns';
export default function ConfigureContent() {
  const { sampleId } = useParams();
  // const [network, setNetwork] = useState('a');
  // const [priority, setPriority] = useState('a');
  const [platForm, setPlatForm] = useState('');
  const [arch, setArch] = useState('');
  const [platFormChild, setPlatFormChild] = useState('');
  const [archChild, setArchChild] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSpecific, setIsSpecific] = useState(false);
  const [currentChild, setCurrentChild] = useState(0);
  const [listChild, setListChild] = useState([]);
  // const [low, setLow] = useState({ backgroundColor: 'rgba(0, 255, 30, .2)', color: '#009912' });
  // const [medium, setMedium] = useState({ backgroundColor: 'rgba(255, 215, 0, .2)', color: '#998100' });
  // const [high, setHigh] = useState({ backgroundColor: 'rgba(215, 88, 111, .2)', color: '#a2273d' });
  const history = createBrowserHistory();
  const [fileChild, setFileChild] = useState(false);
  const result = JSON.parse(localStorage.getItem('fileData'));
  const listDataUpload = result.length > 1 ? result : [result];
  const pathName =
    listDataUpload[0].isParent === 1
      ? listDataUpload[0].fileName
      : listDataUpload[0].fileName;
  function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i === 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
  }
  const [fileInfo, setFileInfo] = useState({
    name: pathName,
    path: listDataUpload[0].pathFile,
    type: listDataUpload[0].fileType,
    mime: listDataUpload[0].fileType,
    size: listDataUpload[0].fileSize,
  });
  // const changePriority = event => {
  //   const { value, checked } = event.target;
  //   setPriority(value);
  //   value === 'a' && checked
  //     ? setLow({ backgroundColor: '#00cc18', color: '#fff' })
  //     : setLow({ backgroundColor: 'rgba(0, 255, 30, .2)', color: '#009912' });
  //   value === 'b' && checked
  //     ? setMedium({ backgroundColor: '#ccac00', color: '#fff' })
  //     : setMedium({ backgroundColor: 'rgba(255, 215, 0, .2)', color: '#998100' });
  //   value === 'c' && checked
  //     ? setHigh({ backgroundColor: '#cb314d', color: '#fff' })
  //     : setHigh({ backgroundColor: 'rgba(215, 88, 111, .2)', color: '#a2273d' });
  // };
  const showChild = () => {
    if (listDataUpload.length > 1) {
      setFileChild(true);
    }
  };
  const showInfoFile = async id => {
    setIsSpecific(true);
    setCurrentChild(id);
      try {
        let res = await FileService.getSampleById(id);
        setPlatFormChild(res.data.flatform);
        setArchChild(res.data.arch);
      } catch (error) {
        console.log(error);
      }
    
    var arrFilter = listDataUpload.filter(item => item.id === id);
    setFileInfo(prevState => ({
      ...prevState,
      name: arrFilter[0].fileName,
      path: arrFilter[0].pathFile,
      type: arrFilter[0].fileType,
      size: arrFilter[0].fileSize,
    }));
  };

  const changePlatForm = event => {
    setPlatForm(event);
  };
  const changeArch = event => {
    setArch(event);
  };
  const changePlatFormChild = async event => {
    var data = {
      arch: archChild,
      os: event,
    }
    try {
      let res = await FileService.updateArchById(currentChild, data);
    } catch (error) {
      console.log(error);
    }
    setPlatFormChild(event);
  };
  const changeArchChild = async event => {
    var data = {
      arch: event,
      os: platFormChild,
    }
    try {
      let res = await FileService.updateArchById(currentChild, data);
    } catch (error) {
      console.log(error);
    }
    setArchChild(event);
  };
  const handleAnalyst = async () => {
    var arraySample = [];
    setIsLoading(true);
    listDataUpload.forEach(element => {
      arraySample.push({ id: element.id });
    });
    var settingObj = {
      target: 'ABC',
      category: 'category',
      timeout: 1,
      priority: '2',
      custom: 'ABC',
      machine: 'Window',
      task_package: 'ABC',
      options: 'ABC',
      platform: platForm,
      memomy: 2,
      arch: arch,
      package: 's',
      addedOn: '2023-01-04T07:59:43.990+0000',
      startedOn: '2023-01-04T07:59:43.990+0000',
      completedOn: null,
      samples: arraySample,
      enforceTimeout: '2',
      status: 'pending',
    };
    try {
      const response = await axios({
        method: 'POST',
        url: `${baseURL}task/createTask`,
        data: JSON.stringify(settingObj),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
      });
      setTimeout(() => {
        setIsLoading(false);
      }, 2000);
      localStorage.setItem('idTag', response.data.data.id);
      history.push({ pathname: `${rootPath.pending}/${sampleId}` });
      history.go({ pathname: `${rootPath.pending}/${sampleId}` });
    } catch (error) {
      console.log(error);
    }
  };
  const callApiGetSample = async () => {
    const response = await axios({
      method: 'GET',
      url: `${baseURL}/samples/${sampleId}`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
    });
    setPlatForm(response.data.flatform? response.data.flatform: "windows")
    setArch(response.data.arch? response.data.arch: "x86")
  }
  useEffect(() => {
    callApiGetSample();
},[]);

  return (
    <>
      {
        isLoading?
        <div className={styles.loadingContainer}>
        <div className={styles.loadingSpinner}>
          <RotatingLines strokeColor="grey" strokeWidth="5" animationDuration="0.75" width="96" visible={true} />
        </div>
      </div>
      :
      ""
      }
   
      <div
        className="container"
        style={{
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          width: '100vw',
          height: '100vh',
        }}>
        <div className={styles.buttonContainer}>
          <h3 className={styles.mainTitle}>
            <SettingOutlined style={{ marginRight: '5px', fontSize: '24px' }} /> Configure your Analysis
          </h3>
          <div>
            {/* <button className={styles.btnOption} id={styles.resetOption}>
              {' '}
              <RedoOutlined /> Reset
            </button> */}
            <button className={styles.btnOption} id="start-analysis" onClick={handleAnalyst}>
              <CheckOutlined /> Analyze
            </button>
          </div>
        </div>
        <div className={styles.formBody}>
          <div id={styles.configForm}>
            <div className={styles.formHeader}>
              <h4 className={styles.titleHeader}>
                <GlobalOutlined className={styles.anticonGlobal} />
                Global Advanced Options
              </h4>
              <small className={styles.desHeader}>
                Options you change here are globally persisted to all files in your selection.
              </small>
            </div>
            <form id="submission-config">
              {/* <div className={styles.formItem}>
                  <div className={styles.formTitle}>Network Routing</div>
                  <div className="radioGroup">
                    <Radio.Group className={styles.radioGroup} onChange={networkChange} value={network} buttonStyle="solid">
                      <Radio.Button className={styles.radioStyle} value="a">
                      Windows
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="b">
                      MacOS
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="c">
                      Linux
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="d">
                      Android
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="d">
                      BSD
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="d">
                      DOS
                      </Radio.Button>
                    </Radio.Group>
                  </div>
                </div> */}

              {/* <div className={styles.formItem}>
                  <div className={styles.formTitle}>Time out</div>
                  <div className="radioGroup">
                    <Radio.Group className={styles.radioGroup} defaultValue="a" buttonStyle="solid">
                      <Radio.Button className={styles.radioStyle} value="a">
                        60
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="b">
                        120
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} value="c">
                        300
                      </Radio.Button>
                    </Radio.Group>
                  </div>
                </div> */}

              {/* <div className={styles.formItem}>
                  <div className={styles.formTitle}>Priority</div>
                  <div className="radioGroup">
                    <Radio.Group style={{ display: 'flex' }} value={priority} onChange={changePriority}>
                      <Radio.Button className={styles.radioStyle} style={low} value="a">
                        LOW
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} style={medium} value="b">
                        MEDIUM
                      </Radio.Button>
                      <Radio.Button className={styles.radioStyle} style={high} value="c">
                        HIGH
                      </Radio.Button>
                    </Radio.Group>
                  </div>
                </div> */}
              {/* <div className={styles.formItem}>
                  <div className={styles.formTitle}>Options</div>
                  <ul className={styles.toggleList}>
                    <li>
                      <p>
                        Remote Control <span>Enables Guacamole UI for VM</span>
                      </p>
                      <Switch defaultChecked className={{ checked: styles.checkboxtestworking }} />
                    </li>
                    <li>
                      <p>
                        Enable Injection <span>Enable behavioral analysis.</span>
                      </p>
                      <Switch defaultChecked />
                    </li>
                    <li>
                      <p>Process Memory Dump </p>
                      <Switch defaultChecked />
                    </li>
                  </ul>
                </div> */}
              <div className={styles.formItem}>
                <div className={styles.formTitle}>Arch</div>
                <div className="radioGroup">
                  <Select
                    style={{ width: '100%' }}
                    onChange={changeArch}
                    value={arch}
                    options={[
                      {
                        value: '8086',
                        label: '8086',
                      },
                      {
                        value: 'x86',
                        label: 'x86',
                      },
                      {
                        value: 'x64',
                        label: 'x64',
                      },
                      {
                        value: 'x8664',
                        label: 'x8664',
                      },
                      {
                        value: 'ARM',
                        label: 'ARM',
                      },
                      {
                        value: 'ARM64',
                        label: 'ARM64',
                      },
                      {
                        value: 'MIPS',
                        label: 'MIPS',
                      },
                      {
                        value: 'PowerPC',
                        label: 'PowerPC',
                      },
                    ]}
                  />
                </div>
              </div>
              <div className={styles.formItem}>
                <div className={styles.formTitle}>Platform</div>
                <div className="radioGroup">
                  <Select
                    style={{ width: '100%' }}
                    onChange={changePlatForm}
                    value={platForm}
                    options={[
                      {
                        value: 'windows',
                        label: 'Windows',
                      },
                      {
                        value: 'macos',
                        label: 'MacOS',
                      },
                      {
                        value: 'linux',
                        label: 'Linux',
                      },
                      {
                        value: 'android',
                        label: 'Android',
                      },
                      {
                        value: 'bsd',
                        label: 'BSD',
                      },
                      {
                        value: 'dos',
                        label: 'DOS',
                      }
                    ]}
                  />
                </div>
              </div>
            </form>
          </div>
          <div className={styles.configContent}>
            <div className={styles.fileControl}>
              {/* <ul>
                <li>
                  <a href="/" title="Expand all folders">
                    <FolderOpenOutlined />
                  </a>
                </li>
                <li>
                  <a href="/" title="Collapse all folders">
                    <FolderOutlined />
                  </a>
                </li>
                <li className={styles.intend}>
                  <a href="/" title="Select all files">
                    <CheckSquareOutlined />
                  </a>
                </li>
                <li>
                  <a href="/" title="Deselect all files">
                    <BorderOutlined />
                  </a>
                </li>
              </ul> */}
              {/* <ul className={styles.right}>
                <li>
                  <p>
                    <a href="filetree:showSelection">
                      Selection: <span data-value="1">1</span>/<span data-value="filetree:totalFilesCount">1</span>
                    </a>
                  </p>
                </li>
              </ul> */}
            </div>
            <div className={styles.fileContent}>
              <div id={styles.fileTree}>
                <ul>
                  {
                    <li key={listDataUpload[0].id} onClick={showChild}>
                      <div className={styles.wrapItem}>
                        <div style={{ display: 'flex', float: 'left', alignItems: 'center' }}>
                          {listDataUpload[0].isParent === 1 ? <FileZipOutlined /> : ''}
                          <span style={{ marginLeft: '9px', display: 'block' }}>{pathName}</span>
                        </div>
                        <div>
                          <a className={styles.labelInfo}>
                            <InfoCircleOutlined />
                          </a>
                          <span className={styles.labelInfo}>{bytesToSize(listDataUpload[0].fileSize)}</span>
                        </div>
                      </div>
                      <ul className={styles.ulChild} style={{ display: fileChild ? 'block' : 'none' }}>
                        {listDataUpload.map((item, idx) => {
                          return item.isParent !== 1 ? (
                            <li key={item.id} style={{ padding: "0px" }} onClick={() => showInfoFile(item.id)}>
                              <div className={styles.wrapItem}>
                                <div style={{ display: 'flex', float: 'left', alignItems: 'center' }}>
                                  <span style={{ marginLeft: '9px', display: 'block' }}>{item.fileName}</span>
                                </div>
                                <div>
                                  <a className={styles.labelInfo}>
                                    <InfoCircleOutlined />
                                  </a>
                                  <span className={styles.labelInfo}>{bytesToSize(item.fileSize)}</span>
                                </div>
                              </div>
                            </li>
                          ) : (
                            ''
                          );
                        })}
                      </ul>
                    </li>
                  }
                </ul>
              </div>
              <div id={styles.fileDetail}>
                <div id={styles.configForm}>
                  <div className={styles.formHeader}>
                    <h4 className={styles.titleHeader}>
                      <FileOutlined className={styles.anticonGlobal} />
                      {fileInfo.name}
                    </h4>
                  </div>
                  <div className={styles.fileInfo}>
                    <ul>
                      {/* <li>
                        <strong> path </strong>
                        {fileInfo.path}
                      </li> */}
                      <li>
                        <strong>type</strong>
                        {fileInfo.type}
                      </li>
                      <li>
                        <strong>size</strong>
                        {bytesToSize(fileInfo.size)}
                      </li>
                    </ul>
                  </div>
                </div>
                <div id={styles.configForm} style={{ display: isSpecific ? "block" : "none" }}>
                  <div className={styles.formHeader}>
                    <h4 className={styles.titleHeader}>
                      <GlobalOutlined className={styles.anticonGlobal} />
                      Analysis Specific options
                    </h4>
                    <small className={styles.desHeader}>
                      Options you change here are globally persisted to all files in your selection.
                    </small>
                  </div>
                  <form id="submission-config">
                    {/* <div className={styles.formItem}>
                        <div className={styles.formTitle}>Network Routing</div>
                        <div className="radioGroup">
                          <Radio.Group className={styles.radioGroup} value={network} buttonStyle="solid">
                            <Radio.Button className={styles.radioStyle} value="a">
                              NONE
                            </Radio.Button>
                            <Radio.Button className={styles.radioStyle} value="b">
                              INTERNET
                            </Radio.Button>
                            <Radio.Button className={styles.radioStyle} value="c">
                              INETSIM
                            </Radio.Button>
                            <Radio.Button className={styles.radioStyle} value="d">
                              TOR
                            </Radio.Button>
                          </Radio.Group>
                        </div>
                      </div> */}
                    {/* <div className={styles.formItem}>
                        <div className={styles.formTitle}>Priority</div>
                        <div className="radioGroup">
                          <Radio.Group style={{ display: 'flex' }}>
                            <Radio.Button className={styles.radioStyle} style={low} value="a">
                              LOW
                            </Radio.Button>
                            <Radio.Button className={styles.radioStyle} style={medium} value="b">
                              MEDIUM
                            </Radio.Button>
                            <Radio.Button className={styles.radioStyle} style={high} value="c">
                              HIGH
                            </Radio.Button>
                          </Radio.Group>
                        </div>
                      </div> */}
                    {/* <div className={styles.formItem}>
                        <div className={styles.formTitle}>Options</div>
                        <ul className={styles.toggleList}>
                          <li>
                            <p>
                              Remote Control <span>Enables Guacamole UI for VM</span>
                            </p>
                            <Switch defaultChecked className={{ checked: styles.checkboxtestworking }} />
                          </li>
                          <li>
                            <p>
                              Enable Injection <span>Enable behavioral analysis.</span>
                            </p>
                            <Switch defaultChecked />
                          </li>
                          <li>
                            <p>Process Memory Dump </p>
                            <Switch defaultChecked />
                          </li>
                        </ul>
                      </div> */}
                    <div className={styles.formItem}>
                      <div className={styles.formTitle}>Arch</div>
                      <div className="radioGroup">
                        <Select
                          style={{ width: '100%' }}
                          onChange={changeArchChild}
                          value={archChild}
                          options={[
                            {
                              value: '8086',
                              label: '8086',
                            },
                            {
                              value: 'x86',
                              label: 'x86',
                            },
                            {
                              value: 'x64',
                              label: 'x64',
                            },
                            {
                              value: 'x8664',
                              label: 'x8664',
                            },
                            {
                              value: 'ARM',
                              label: 'ARM',
                            },
                            {
                              value: 'ARM64',
                              label: 'ARM64',
                            },
                            {
                              value: 'MIPS',
                              label: 'MIPS',
                            },
                            {
                              value: 'PowerPC',
                              label: 'PowerPC',
                            },
                          ]}
                        />
                      </div>
                    </div>
                    <div className={styles.formItem}>
                      <div className={styles.formTitle}>Platform</div>
                      <div className="radioGroup">
                        <Select
                          style={{ width: '100%' }}
                          value={platFormChild}
                          onChange={changePlatFormChild}
                          options={[
                            {
                              value: 'windows',
                              label: 'Windows',
                            },
                            {
                              value: 'macos',
                              label: 'MacOS',
                            },
                            {
                              value: 'linux',
                              label: 'Linux',
                            },
                            {
                              value: 'android',
                              label: 'Android',
                            },
                            {
                              value: 'bsd',
                              label: 'BSD',
                            },
                            {
                              value: 'dos',
                              label: 'DOS',
                            }
                          ]}
                        />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
