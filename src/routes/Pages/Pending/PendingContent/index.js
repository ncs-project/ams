import React, { useEffect, useState } from 'react';
import { CheckOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import styles from '../pending.module.scss';
import '../index.scss';
import axios from 'axios';
import { rootPath } from 'helpers/buildUrl';
import { createBrowserHistory } from 'history';
import { baseURL } from 'services/ConfigService/APIServiceAMS';
import { CalendarOutlined, ClockCircleOutlined } from '@ant-design/icons';
export default function PendingContent() {
  const [listSample, setListSample] = useState([]);
  const [data, setData] = useState({});
  const history = createBrowserHistory();
  const idTag = localStorage.getItem('idTag');

  function formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      hour = d.getHours(),
      minute = d.getMinutes();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    var date = [day, month, year].join('/');
    var time = [hour, minute].join(':');
    return (
      <>
        <CalendarOutlined style={{ paddingRight: '5px' }} />
        {date}
        <ClockCircleOutlined style={{ padding: '0px 5px' }} />
        {time}
      </>
    );
  }
  const columns = [
    {
      title: 'Sample ID',
      dataIndex: 'sampleId',
      key: 'sampleId',
    },
    {
      title: 'Date',
      key: 'date',
      dataIndex: 'date',
    },
    {
      title: 'Filename',
      dataIndex: 'filenameUrl',
      key: 'filenameUrl',
    },
    {
      title: 'File Size',
      key: 'fileSize',
      dataIndex: 'fileSize',
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
    },
  ];
  function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i === 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
  }
  var row = [];

  listSample.forEach(function(element, index) {
    let itemStatus = '';
    if (element.isAnalyse === 0) {
      itemStatus = 
        <>
          {' '}
          <span class="status status-running"></span> Pending{' '}
        </>
    } else if(element.isAnalyse === 1){
      itemStatus = 
        <>
          {' '}
          <span class="status status-reported">✓</span> Success{' '}
        </>
    } else{
      itemStatus = 
        <>
          {' '}
          <span class="status status-error">✕</span> Error{' '}
        </>
    }
    if (element.isParent !== 1) {
      row.push({
        key: index,
        sampleId: element.id,
        date: formatDate(data.startedOn),
        filenameUrl: element.fileName,
        fileSize: bytesToSize(element.fileSize),
        fidCoreLogApi: element.fidCoreLogApi,
        fidCoreLogBasic: element.fidCoreLogBasic,
        isAnalyse: element.isAnalyse,
        // fileType: element.fileType,
        status: itemStatus ,
      });
    }
  });
  const analyzeData = async () => {
    try {
      const response = await axios.get(`${baseURL}/tasks/${idTag}`);
      var status = response.data.status;
      if (status === 'success') {
        debugger;
        setData(response.data);
        setListSample(response.data.samples);
      } else {
        debugger;
        setData(response.data);
        setListSample(response.data.samples);
        setTimeout(analyzeData, 2500);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    analyzeData();
  }, []);

  return (
    <>
      <div
        className="container"
        style={{
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          width: '100vw',
          height: '100vh',
        }}>
        <div className={styles.headerContainer}>
          <h3>
            <CheckOutlined style={{ marginRight: '5px', fontSize: '24px' }} /> Your submission has been received and the
            tasks are being processed
          </h3>
        </div>
        <div className={styles.bodyContainer}>
          <div className={styles.bodyContent}>
            <div className={styles.bodyColumn}>
              {/* <h3>
                Tasks: <small>Refreshes every 2.5 seconds</small>
              </h3> */}
              <Table
                onRow={(record, rowIndex) => {
                  return {
                    onClick: event => {
                      if (record.isAnalyse === 1) {
                        var objFileInfo = {
                          currentSample: record.sampleId,
                          fileSize: record.fileSize,
                          fileName: record.filenameUrl,
                          fileType: record.fileType,
                          completedOn: data.completedOn,
                          fidCoreLogApi: record.fidCoreLogApi,
                          fidCoreLogBasic: record.fidCoreLogBasic,
                        };
                        localStorage.setItem('currentFile', JSON.stringify(objFileInfo));
                        history.push({ pathname: `${rootPath.file}/${record.sampleId}/details` });
                        history.go({ pathname: `${rootPath.file}/${record.sampleId}/details` });
                      }
                    },
                  };
                }}
                columns={columns}
                dataSource={row}
                pagination={{ pageSize: 4, hideOnSinglePage: true }}
                style={{ width: '100%' }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
