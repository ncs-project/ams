import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router';
import PageLoader from '@jumbo/components/PageComponents/PageLoader';

const ExportFile = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  console.log("requestedUrl", requestedUrl)
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route  exact path={`${requestedUrl}/:sampleId`} component={lazy(() => import('./PendingContent'))} />
        <Route component={lazy(() => import('../404'))} />
      </Switch>
    </Suspense>
  );
};
export default ExportFile;