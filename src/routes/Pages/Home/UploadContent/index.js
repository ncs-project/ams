import React, { useState } from 'react';
import { Col, Row, Button, Upload, Tabs } from 'antd';
import logo from '../../../../assset/images/logo.svg';
import file from '../../../../assset/images/file.svg';
import styles from '../upload.module.scss';
import '../index.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { rootPath } from 'helpers/buildUrl';
import axios from 'axios';
import ModalUpload from './ModalUpload';
import {baseURL} from 'services/ConfigService/APIServiceAMS';
export default function ListUser() {
  // const { file } = useSelector(reduxData => reduxData.file);
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const location = useLocation();
  const [isUpload, setIsUpLoad] = useState(false);
  const [fileList, setFileList] = useState([]);
  const urlTab = location.pathname.split('/')[2];
  const [textUpload, setTextUpload] = useState('Choose File');
  const [textUploadPopUp, setTextUploadPopUp] = useState('Choose File');
  const props = {
    beforeUpload: async fileSelect => {
      setIsUpLoad(true);
      setFileList([...fileList, fileSelect]);
      const formData = new FormData();
      formData.append('file', fileSelect);
      debugger;
      const config = {
        onUploadProgress: progressEvent => {
          const { loaded, total } = progressEvent;
          let percent = Math.floor((loaded * 100) / total);

          if (open) {
            setTextUploadPopUp(`Uploading... ${percent}%`);
          } else {
            setTextUpload(`Uploading... ${percent}%`);
          }
          console.log(`${loaded}kb of ${total}kb | ${percent}%`);
        },
      };
      axios
        .post(`${baseURL}seaweedfs/uploadFile`, formData, config)
        .then(res => {
          if (res.status === 200) {
              debugger;
            var data = res.data;
            var tagId = data.id ? data.id : data[0].id;
            if (open) {
              setTextUploadPopUp(`Upload success!`);
            } else {
              setTextUpload(`Upload success!`);
            }
            localStorage.setItem('fileData', JSON.stringify(res.data));
            setTimeout(() => goToConfig(tagId, data), 1000);
          }
        })
        .catch(err => {
          if (open) {
            setTextUploadPopUp(`Upload fail`);
          } else {
            setTextUpload(`Upload fail`);
          }
          console.log('error: ', err.message);
        });
    },
    fileList,
  };
  const changeTab = key => {
    history.push(key);
  };
  const handleDrag = () => {
    showModal();
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const showModal = () => {
    setOpen(true);
  };
  
  const goToConfig = (tagId, data) => {
    debugger;
    if(data.isAnalyse === 1){
      history.push({ pathname: `${rootPath.file}/${tagId}/details` });
    }
    else{
      history.push({ pathname: `${rootPath.configure}/${tagId}` });
    }

  };
  return (
    <>
      <div className="container" style={{ minHeight: "100vh" }}  onDragEnter={handleDrag}>
        <Row style={{ display: 'flex', justifyContent: 'center', paddingTop: '1.5rem', maxWidth: '700px', margin: 'auto' }}>
          <div className={styles.mainContent}>
            <Row>
              <div className={styles.logoContainer}>
                <img src={logo} style={{ width: '250px', height: 'auto', marginBottom: '10px' }} alt="logo" />
              </div>
              {/* <Col span={18} style={{ textAlign: 'center', margin: 'auto' }}>
                <p>
                  Analyse suspicious files, domains, IPs and URLs to detect malware and other breaches, automatically share
                  them with the security community.
                </p>
              </Col> */}
              <Col span={24}>
                <Tabs
                  defaultActiveKey={urlTab}
                  onChange={changeTab}
                  items={[
                    {
                      label: 'FILE',
                      key: 'upload',
                    },
                    // {
                    //   label: 'URL',
                    //   key: 'url',
                    // },
                    {
                      label: 'SEARCH',
                      key: 'search',
                    },
                  ]}
                />
              </Col>
              <div className={styles.actionContent} onDragEnter={handleDrag}>
                <div className={styles.actionForm}>
                  <div className={styles.draggerContainer} onDragEnter={handleDrag} draggable={true}>
                    <div className={styles.wrap}>
                      <img src={file} alt="upload" style={{ textAlign: 'center' }} />
                      {
                        // <Dragger
                        //   {...props}
                        //   // beforeUpload={() => {
                        //   //   /* update state here */
                        //   //   return false;
                        //   // }}
                        //   showUploadList={false}
                        //   openFileDialogOnClick={false}
                        //   // style={{ display: open ? 'unset' : 'none' }}
                        //   className={styles.draggerContent}></Dragger>
                      }
                      <Upload {...props}>
                        <Button className={styles.buttonUpload}>{textUpload}</Button>
                      </Upload>
                    </div>
                  </div>

                  {/* <div className={styles.description}>
                    By submitting data above, you are agreeing to our
                    <a
                      className={styles.blueLink}
                      href="#">
                      Terms of Service
                    </a>{' '}
                    and{' '}
                    <a
                      className={styles.blueLink}
                      href="#">
                      Privacy Policy
                    </a>
                    , and to the <strong>sharing of your Sample submission with the security community.</strong> Please do
                    not submit any personal information; NCS is not responsible for the contents of your submission.
                    <a
                      className={styles.blueLink}
                      href="#">
                      Learn more.
                    </a>
                  </div> */}
                </div>
              </div>
            </Row>
          </div>
        </Row>
      </div>
      <ModalUpload props={props} textUploadPopUp={textUploadPopUp} open={open} handleCancel={handleCancel} />
    </>
  );
}
