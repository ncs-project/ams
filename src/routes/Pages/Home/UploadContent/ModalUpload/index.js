import {useRef} from 'react';
import React from 'react';
import { Button, Upload, Modal } from 'antd';
import styles from '../../upload.module.scss';
import '../../index.scss';
import file from '../../../../../assset/images/file.svg';
import useOutsideClick from "./useOutsideClick";
export default function ModalUpload({ props, textUploadPopUp, open, handleCancel }) {
  const Dragger = Upload.Dragger;

 const ref = useRef();

  useOutsideClick(ref, () => {
    if(open){
      handleCancel();
    }
  });

  return (
    <>
      <Dragger
        onClick={()=>{console.log(12)}}
        className={styles.draggerContent}
        // id="draggerContent"
        {...props}
        // ondrop={props}
        style={{ visibility: open ? 'visible' : 'hidden' }}
        openFileDialogOnClick={false}
        onRemove={handleCancel}
        showUploadList={false}>
        </Dragger>
      <div className="container" >
        <Modal title="" open={open} footer={null} >
          <div className={styles.actionContent} ref={ref}>
            <div className={styles.actionForm}>
              <div
                className={styles.draggerContainer}
                // ondrop={props}
                draggable={true}
                style={{ width: '1000px', padding: '25px 0px' }}>
                <div className={styles.wrap}>
                  <img src={file} alt="upload" style={{ textAlign: 'center' }} />
                  {
                    <div className="drag-wrap">
                      {/* <Dragger
                        {...props}
                        onClick={() => {console.log(111)}}
                        openFileDialogOnClick={false}
                        showUploadList={false}
                        className={styles.draggerContent}>
                        </Dragger> */}
                    </div>
                  }
                  <Upload {...props}>
                    <Button className={styles.buttonUpload}>{textUploadPopUp}</Button>
                  </Upload>
                </div>
                {/* <div className={styles.description}>
                    By submitting data above, you are agreeing to our
                    <a
                      className={styles.blueLink}
                      href="#">
                      Terms of Service
                    </a>{' '}
                    and{' '}
                    <a
                      className={styles.blueLink}
                      href="#">
                      Privacy Policy
                    </a>
                    , and to the <strong>sharing of your Sample submission with the security community.</strong> Please do
                    not submit any personal information; NCS is not responsible for the contents of your submission.
                    <a
                      className={styles.blueLink}
                      href="https://support.virustotal.com/hc/en-us/articles/115002126889-How-it-works">
                      Learn more.
                    </a>
                  </div>  */}
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
