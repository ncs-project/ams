import React, {useState} from 'react';
import { Col, Row, Input, Tabs, Tooltip } from 'antd';
import logo from '../../../../assset/images/logo.svg';
import search from '../../../../assset/images/search.png';
import styles from '../upload.module.scss';
import '../index.scss';
import { useHistory, useLocation  } from "react-router-dom";
import { rootPath } from 'helpers/buildUrl';
export default function ListUser() {
  const history = useHistory();
  const location = useLocation();
  const urlTab = location.pathname.split("/")[2];
  const [key, setKey] = useState("");
  const handleChange = (e) => {
    setKey(e.target.value);
  }
  const changeTab = (key) => {
    history.push(key);
  }
  const handleKeyPress = (e) => {
    if(e.key === 'Enter' && key){
      history.replace({ pathname: `${rootPath.search}/${key}` });
    }
  }
  const handleSearch = (e) => {
    if(key)
      history.replace({ pathname: `${rootPath.search}/${key}` });
  }
  return (
    <>
      <div className="container"  style={{ minHeight: "100vh" }}>
        <Row style={{ display: 'flex', justifyContent: 'center', paddingTop: '1.5rem', maxWidth: '700px', margin: 'auto' }}>
          <div className={styles.mainContent}>
            <Row>
              <div className={styles.logoContainer}>
              <img src={logo} style={{ width: "250px", height: "auto", marginBottom: "10px"}} alt="logo" />
              </div>
              {/* <Col span={18} style={{ textAlign: 'center', margin: 'auto' }}>
                <p>
                  Analyse suspicious files, domains, IPs and URLs to detect malware and other breaches, automatically share
                  them with the security community.
                </p>
              </Col> */}
              <Col span={24}>
                <Tabs
                  defaultActiveKey={urlTab}
                  onChange={changeTab}
                  items={[
                    {
                      label: 'FILE',
                      key: 'upload',
                    },
                    // {
                    //   label: 'URL',
                    //   key: 'url',
                    // },
                    {
                      label: 'SEARCH',
                      key: 'search',
                    },
                  ]}
                />
              </Col>
              <div className={styles.actionContent}>
                <div  className={styles.actionForm}>
                  <Tooltip title="Search">
                  <img style={{maxWidth: "100px", height: "auto",  cursor: "pointer"}}src={search} alt="searchs" onClick={handleSearch} />
                  </Tooltip>
                  <Input onKeyPress={ handleKeyPress } onChange={handleChange} placeholder="richPeHeaderHash, md5, tlsh, sha1, sha256" style={{ padding: "0.5rem 1.5rem"}}  />
                  {/* <div className={styles.description}>
                    By submitting data above, you are agreeing to our
                    <a
                      className={styles.blueLink}
                      href="https://support.virustotal.com/hc/en-us/articles/115002145529-Terms-of-Service">
                      Terms of Service
                    </a>{' '}
                    and{' '}
                    <a className={styles.blueLink} href="https://support.virustotal.com/hc/en-us/articles/115002168385-Privacy-Policy">
                      Privacy Policy
                    </a>
                    , and to the <strong>sharing of your Sample submission with the security community.</strong> Please do
                    not submit any personal information; VirusTotal is not responsible for the contents of your submission.
                    <a className={styles.blueLink} href="https://support.virustotal.com/hc/en-us/articles/115002126889-How-it-works">
                      Learn more.
                    </a>
                  </div> */}
                </div>
              </div>
            </Row>
          </div>
        </Row>
      </div>
    </>
  );
}
