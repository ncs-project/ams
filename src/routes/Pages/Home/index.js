import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router';
import PageLoader from '@jumbo/components/PageComponents/PageLoader';

const Home = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route exact path={`${requestedUrl}/upload`} component={lazy(() => import('./UploadContent'))} />
        <Route path={`${requestedUrl}/url`} component={lazy(() => import('./UrlContent'))} />
        <Route path={`${requestedUrl}/search`} component={lazy(() => import('./SearchContent'))} />
        <Route component={lazy(() => import('../404'))} />
      </Switch>
    </Suspense>
  );
};
export default Home;
