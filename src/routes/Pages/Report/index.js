import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '@jumbo/components/PageComponents/PageLoader';

const Report = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  console.log("requestedUrl", requestedUrl)
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route  path={`${requestedUrl}`} component={lazy(() => import('./ReportContent/ReportContent'))} />
        <Route component={lazy(() => import('../404'))} />
      </Switch>
    </Suspense>
  );
};
export default Report;
