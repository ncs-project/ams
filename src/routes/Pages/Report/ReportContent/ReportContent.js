import React from 'react';
import Metadata from './Content/Metadata';
import TreeProcess from './Content/TreeProcess';
import styles from './Report.module.scss';

const ReportContent = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Report</h1>

      <div className={styles.wrapper}>
        <h2>Process tree</h2>
        <div className={styles.content}>
          <TreeProcess />
        </div>
      </div>

      <div className={styles.wrapper}>
        <h2>Meta data</h2>
        <div className={styles.content}>
          <Metadata />
        </div>
      </div>
    </div>
  );
};

export default ReportContent;
