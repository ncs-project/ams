import React from 'react';
import {
  DownOutlined,
  FrownFilled,
  FrownOutlined,
  MehOutlined,
  PlusCircleFilled,
  MinusCircleFilled,
} from '@ant-design/icons';
import { Tree } from 'antd';
import styles from './TreeProcess.module.scss';

const treeData = [
  {
    title: (
      <>
        <span className={styles.titleName}>
          unnamed process<a>(0)</a>
        </span>
      </>
    ),
    key: '0',
    icon: <PlusCircleFilled />,
  },
  {
    title: (
      <>
        <span className={styles.titleName}>
          unnamed process<a>(368)</a>
        </span>
      </>
    ),
    key: '1',
    // icon: ({ selected }) => (selected ? <FrownFilled /> : <FrownOutlined />),
    icon: <MinusCircleFilled />,
    children: [
      {
        title: (
          <>
            <span className={styles.titleName}>
              csrss.exe<a>(384)</a>
            </span>
          </>
        ),
        key: '1-1',
        icon: <MinusCircleFilled />,
        children: [
          {
            title: (
              <>
                <span className={styles.titleName}>
                  conhost.exe<a>(2716)</a>
                </span>
              </>
            ),
            key: '1-1-1',
          },
        ],
      },
      {
        title: (
          <>
            <span className={styles.titleName}>
              winlogon.exe<a>(424)</a>
            </span>
          </>
        ),
        key: '1-2',
      },
    ],
  },
  {
    title: (
      <>
        <span className={styles.titleName}>
          unnamed process<a>(1456)</a>
        </span>
      </>
    ),
    key: '2',
    icon: <MinusCircleFilled />,
    children: [
      {
        title: (
          <>
            <span className={styles.titleName}>
              explorer.exe<a>(1544)</a>
            </span>
          </>
        ),
        key: '2-1',
        icon: <MinusCircleFilled />,
        children: [
          {
            title: (
              <>
                <span className={styles.titleName}>
                  cmd.exe<a>(1384)</a>
                </span>
              </>
            ),
            key: '2-1-1',
          },
          {
            title: (
              <>
                <span className={styles.titleName}>
                  test.exe<a>(1620)</a>
                </span>
              </>
            ),
            key: '2-1-2',
          },
        ],
      },
    ],
  },
];

const TreeProcess = () => {
  return (
    <Tree showIcon defaultExpandAll defaultSelectedKeys={['0-0-0']} switcherIcon={<DownOutlined />} treeData={treeData} />
  );
};

export default TreeProcess;
