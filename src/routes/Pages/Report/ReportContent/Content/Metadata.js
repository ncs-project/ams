import React from 'react';
import styles from '../Content/Metadata.module.scss';

const Metadata = () => {
  return (
    <div>
      <div className={styles.wrapper}>
        <div className={styles.content}>
          <div className={styles.container}>
            <div className={styles.wapperTitle}>
              <h2 className={styles.title}>SHA256</h2>
            </div>
            <h2 className={styles.key}>abdsflk3jllsk5lkjdafladfj6643lkjsdfkajsd</h2>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.container}>
            <div className={styles.wapperTitle}>
              <h2 className={styles.title}>Magic bytes</h2>
            </div>
            <h2 className={styles.key}>PE32+ executable (console) x84-64, for MS Windows</h2>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.container}>
            <div className={styles.wapperTitle}>
              <h2 className={styles.title}>Start command</h2>
            </div>
            <h2 className={styles.key}>C:\Users\janusz\Desktop\test.exe</h2>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.container}>
            <div className={styles.wapperTitle}>
              <h2 className={styles.title}>Start at</h2>
            </div>
            <h2 className={styles.key}>2020-11-23 10:41:18</h2>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.container}>
            <div className={styles.wapperTitle}>
              <h2 className={styles.title}>Finished at</h2>
            </div>
            <h2 className={styles.key}>20-11-23 10:42:33</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Metadata;
