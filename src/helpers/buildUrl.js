export const prefix = process.env.REACT_APP_PREFIX ? '/' + process.env.REACT_APP_PREFIX : '';

export const rootPath = {
  signin: prefix + '/signin',
  signup: prefix + '/signup',
  file: prefix + '/file',
  home: prefix + '/home',
  configure: prefix + '/submit/pre',
  search: prefix + '/search',
  download: prefix + '/download',
  export: prefix + '/export',
  pending: prefix + '/submit/post'
};
export const childPath = {
  signin: rootPath.signin,
  signup: rootPath.signup,
  home: rootPath.home,
  file: rootPath.file,
  configure: rootPath.configure,
  search: rootPath.search,
  download: rootPath.download,
  export: rootPath.export,
  pending: rootPath.pending,
};
